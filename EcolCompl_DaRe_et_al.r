## Bayesian Generalized Linear Spatial Model 
## October 2018
## Daniele Da Re, Enrico Tordoni, Giovanni Bacaro

#### Set wd and load pkgs, functions ####
#setwd("/home/users/d/d/ddare/BGLSM_full/")
setwd("/storepelican/ddare/BGLSM_2018/")
# setwd("C:/Users/ddare/Desktop/BGLSM_review/BGLSM_October_2018/BGLSM_2018/")

library(readr)
library(geoRglm)
library(parallel)
library(coda)
library(dplyr)
library(rgeos)
library(raster)
library(rgeos)

#load("workspace20181031.RData"  )

Rmse <- function(error)
{
  sqrt(mean(error^2))
}

MAE <- function(error)
{
  mean(abs(error))
}

#### Load data ####
##load observed data
obs<-shapefile("Aliene_species_richness_TFE.shp")
obs_spdf = gCentroid(obs,byid=TRUE)
obs_df<-over(obs_spdf, obs)
obs_df<-data.frame(obs_df, coordinates(obs_spdf))

#extract covariates
pred_stack<-stack(list.files(pattern=".tif"))
obs_centr<-obs_df
coordinates(obs_centr)<-~ x+y
obs_df2<-data.frame(extract(pred_stack, obs_centr))
obs_df2<-data.frame(coordinates(obs_spdf),obs_df[,1:2], obs_df2)
dati<-obs_df2
names(dati)<- c( "X", "Y", "ID" , "ASR_rich", "Prec_seas", "DEM", "spring_prec", "roads_kernel", "T_min_seas", "T_min" )
dati$x_km<-dati$X/1000
dati$y_km<-dati$Y/1000
dim(dati) #5514 rows
dati<-na.omit(dati)
dim(dati)# 5239 rows 

#scale covariates 
dati$Prec_seas.sc <- as.vector(scale(dati$Prec_seas))
dati$DEM.sc <- as.vector(scale(dati$DEM))
dati$spring_prec.sc <- as.vector(scale(dati$spring_prec))
dati$roads_kernel.sc <- as.vector(scale(dati$roads_kernel))
dati$T_min_seas.sc <- as.vector(scale(dati$T_min_seas))
dati$T_min.sc <- as.vector(scale(dati$T_min))
#dati$T_min.sc [is.na(centroids$T_min.sc)] <- 0
dati<-dati[,c(1:4, 11:18)]

#sample the 75% of the records to train the model
set.seed(666)
dati %>% sample_frac(.75)-> dati_sampled
dim(dati_sampled) # 3929 rows
#write.csv(dati_sampled, "train_df_20180504.csv")

#get the remaining 25% to test the model at the end
test_df<-anti_join(dati, dati_sampled, by="ID")
dim(test_df) # 1310 
#write.csv(test_df, "test_df_20180504.csv")

##load centroids for forecasting data
centroids <- read_csv("centroidi_qgis_05febbraio.csv")
names(centroids)
centroids_coord<-data.frame(centroids[,c(1:3)])
coordinates(centroids_coord)<-~ X+Y
centroids_df<-data.frame(extract(pred_stack, centroids_coord))
centroids_df<-data.frame(centroids_coord, centroids_df)
centroids<-centroids_df[,c(2:3,5:10)]
names(centroids)<- c( "X", "Y", "Prec_seas", "DEM", "spring_prec", "roads_kernel", "T_min_seas", "T_min" )

centroids$x_km<-centroids$X/1000
centroids$y_km<-centroids$Y/1000
summary(centroids)
centroids$roads_kernel[is.na(centroids$roads_kernel)] <- 0

#scale covariates
centroids$Prec_seas.sc <- as.vector(scale(centroids$Prec_seas))
centroids$DEM.sc <- as.vector(scale(centroids$DEM))
centroids$spring_prec.sc <- as.vector(scale(centroids$spring_prec))
centroids$roads_kernel.sc <- as.vector(scale(centroids$roads_kernel))
centroids$T_min_seas.sc <- as.vector(scale(centroids$T_min_seas))
centroids$T_min.sc <- as.vector(scale(centroids$T_min))

#make coordinate matrix for predicted val 
coord_prev<-data.frame(centroids$x_km, centroids$y_km)
coord_prev<-as.matrix(coord_prev)

###check collinearity among variables ####
cor(dati[,c(4, 7:12)], method = "spearman")
library(corrplot) #https://cran.r-project.org/web/packages/corrplot/vignettes/corrplot-intro.html
M <- cor(scale(dati[,c(4, 7:12)]), method = "spearman", use="complete.obs")
colnames(M)<-c("ASR", "P. seasonality", "Elevation","Spring Precipitation", "Roads Density", "T. min", "T. seasonality" )
rownames(M)<-c("ASR", "P. seasonality", "Elevation","Spring Precipitation", "Roads Density", "T. min", "T. seasonality" )
tiff( "D:/OneDrive - UCL/BGLSM_review/BGLSM_October_2018/imgs_NOV2018/correlation.tiff",height = 10, width = 14, res=300, units="in" )
corrplot(M, method = "number", type="upper", order="original", tl.col="black", tl.srt=45)
dev.off()
#We have correlation: Temperatures are too much correlated to elevation, thus we will exclude them from the further analysis and use only elevation, as a proxy of minimum Temperature

#### GLM ####
####calculate glm for comparison 
pois.glm <- glm(formula = ASR_rich ~ roads_kernel.sc + spring_prec.sc + DEM.sc + Prec_seas.sc , family = "poisson", data = dati_sampled)
#pois.glm2 <- glm(formula = ASR_rich ~ roads_kernel.sc +  DEM.sc + Prec_seas.sc + T_min.sc +T_min_seas.sc, family = "poisson", data = dati_sampled) #model2 is the MAM 
summary(pois.glm)
AIC(pois.glm)
logLik(pois.glm)
#pois$residP <- resid(pois.glm, type = "deviance")

#glm cross validation, source https://gerardnico.com/lang/r/cross_validation ; https://www.r-bloggers.com/calculate-leave-one-out-prediction-for-glm/
#interpretation https://stats.stackexchange.com/questions/48766/intepretation-of-crossvalidation-result-cv-glm
require(boot)
glmCV<-cv.glm(dati_sampled,pois.glm)$delta 
pois.glm.beta <- as.numeric(pois.glm$coefficients)

#confidence interval
round(confint(pois.glm),4)

#### geodata and spatial trend ####
#create new geodata
datiXY<-as.geodata(dati_sampled, data.col=4, covar.col=7:10, coords.col = 5:6)
previsionsXY<-as.geodata(centroids, covar.col=11:14, coords.col = 9:10)
summary(datiXY)
summary(previsionsXY)
plot(datiXY)

#observed spatial trend 
TS<- trend.spatial(trend= ~ datiXY$covariate$roads_kernel.sc + datiXY$covariate$spring_prec.sc +   datiXY$covariate$DEM.sc + datiXY$covariate$Prec_seas.sc ,  datiXY)
plot(datiXY, trend = TS)

#predicted spatial trend
TS_prev<-trend.spatial(trend = ~ previsionsXY$covariate$roads_kernel.sc + previsionsXY$covariate$spring_prec.sc + previsionsXY$covariate$DEM.sc + previsionsXY$covariate$Prec_seas.sc,  previsionsXY)
plot(previsionsXY, trend = TS_prev)

#GLSM
MOD <- model.glm.control(trend.d=TS, trend.l=TS_prev, cov.model ="exponential") 

#### variograms ####
variogram<-variog(datiXY, trend=TS, max.dist= 25, uvec=seq(0,25, by=3))
plot(variogram)

lines.variomodel(cov.model="exp",  cov.pars=c(8.0,4.0), nug=3.3, max.dist=25)
fitting<-variofit(variogram, cov.model="exp", ini=c(8.0,4.0), fix.nugget = T, nugget = 3.3)
tiff( "D:/OneDrive - UCL/BGLSM_review/BGLSM_October_2018/imgs_NOV2018/variogram.tiff",height = 8, width = 12, res=300, units="in" )
plot(variogram)
lines(fitting)
dev.off()
#compute final model with trend and spatial autocorrelation
model_fit<-likfit(datiXY, ini=fitting$cov.pars, trend= TS,cov.model = "exponential")     

#lik method ML
summary(model_fit)
pois.glsm.beta<-model_fit$beta

## confidence interval
round(model_fit$beta[2] +  qnorm(c(0.025, 0.975)) * sqrt(model_fit$beta.var[2,2]),4)
round(model_fit$beta[3] +  qnorm(c(0.025, 0.975)) * sqrt(model_fit$beta.var[3,3]),4)
round(model_fit$beta[4] +  qnorm(c(0.025, 0.975)) * sqrt(model_fit$beta.var[4,4]),4)
round(model_fit$beta[5] +  qnorm(c(0.025, 0.975)) * sqrt(model_fit$beta.var[5,5]),4)

###GLSM validation####
validaz<-xvalid(datiXY, model = model_fit) #LOO cross val
tiff( "BGLSM_validaz.tiff",height = 10, width = 14, res=300, units="in" )
par(mfcol=c(5,2),mar=c(3,3,.5,.5),mgp=c(1.5,0.7,0))
plot(validaz) 
par(mfcol=c(1,1))
dev.off()

#### MCMC settings ####
#set2<-list(cov.pars=c(7.5,2.5), beta = 5, family="poisson") #beta= average of cov.pars
set3 <- list(
family='poisson',
trend =trend.spatial(TS, as.data.frame(datiXY$data)),
cov.model='exponential',
cov.pars=c(model_fit$sigmasq, model_fit$phi),
nugget =model_fit$tausq, 
beta=pois.glsm.beta)

mcmc.test2 <- mcmc.control(S.scale = 0.120, thin = 50, n.iter=22000, burn.in = 8000)
test3.tune <- glsm.mcmc(datiXY, coords = datiXY$coords, mcmc.input = mcmc.test2, model = set3, messages=TRUE)
summary(test3.tune$acc.rate)

hist(test3.tune$simulations)
test3.tune$model$beta
#chain diagnostic
require(coda)
test3.tune.c <- create.mcmc.coda(x = test3.tune$simulations[round(runif(1,min = 1, max = nrow(test3.tune$simulations)), 0), ],mcmc.input = mcmc.test2) #

# Plot MCMC samples
par(mfrow=c(2,2))
traceplot(test3.tune.c)
autocorr.plot(test3.tune.c, auto.layout = FALSE)
densplot(test3.tune.c)
geweke.plot(test3.tune.c, auto.layout = FALSE)
par(mfrow=c(1,1))

#### BGLSM settings ####
#prior settings 
strong_prior1<-prior.glm.control(  sigmasq.prior = "fixed", sigmasq = 9.96, phi.prior = "fixed", phi = 2.48) #https://www.stat.washington.edu/peter/591/geoR_sln.html
uniform_prior1<-prior.glm.control(beta.prior =  "flat",  sigmasq.prior = "uniform", phi.prior = "uniform", phi.discrete = c(0.1,4.4), tausq.rel=3.3)

#output
out1<-output.glm.control(sim.posterior = TRUE)

#bayesian pois kriging
test_strict1<- pois.krige.bayes(datiXY, locations = coord_prev, model = MOD, prior = strong_prior1, mcmc.input = mcmc.test2, output = out1)
#save.image("workspace20181113.RData")

#### bayesian output ####
results_strict1<-data.frame(test_strict1$predictive, coord_prev)
#results_strict1<-data.frame(test_strict2$predictive, coord_prev)
summary(results_strict1)

results_strict1$X<-results_strict1$centroids.x_km*1000
results_strict1$Y<-results_strict1$centroids.y_km*1000
results_strict1_geodata<-as.geodata(results_strict1, data.col=2, coords.col=7:8)
plot(results_strict1_geodata) 
plot(results_strict1_geodata, trend =  TS_prev) 
results_strict1_uncertainty<-as.geodata(results_strict1, data.col=3, coords.col=7:8)
plot(results_strict1_uncertainty)

#### GOF ####
###GLM GOF####
pred_glm<-predict(pois.glm, newdata=centroids[,11:14], type= "response" )
summary(pred_glm)

#to raster
pred_glm
pred_glm.spdf<-data.frame(centroids[,9:10], "glm_pred" = pred_glm$round.pred_glm..0.)
summary(pred_glm.spdf)
coordinates(pred_glm.spdf)<-~ x_km + y_km
r.glm_pred<-pred_glm.spdf
gridded(r.glm_pred) <- TRUE 
r.glm_pred <- raster(r.glm_pred)

require(rasterVis)
myBreaks= c(0,3,6,9,12,15,18, 21, 24, 27, 30, 33, 36)
myColorkey <- list(at=myBreaks, ## where the colors change
                   labels=list(labels=myBreaks, ##what to print
                               at=myBreaks))    ##where to print

p1_glm<-levelplot(r.glm_pred, par.settings = YlOrRdTheme, 
                  xlab=NULL, ylab=NULL, scales=list(draw=FALSE),
                  colorkey= myColorkey, at = myBreaks,
                  zscaleLog = NULL, contour = FALSE, margin = F)

p1_glm
tiff("D:/OneDrive - UCL/BGLSM_review/BGLSM_October_2018/imgs_NOV2018/glm_predictions.tiff", height = 4, width = 6, res=300, units="in")
p1_glm
dev.off()

dati_sampled.spdf<-dati_sampled
coordinates(dati_sampled.spdf)<-~ X +Y
crs(dati_sampled.spdf)<-"+proj=utm +zone=28 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0"

test_df.spdf<-test_df
coordinates(test_df.spdf)<-~ X +Y
crs(test_df.spdf)<-"+proj=utm +zone=28 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0"

pred_glm<-data.frame(round(pred_glm, 0), centroids[,c(1:2, 9:10)])
coordinates(pred_glm)<-~ X +Y
crs(pred_glm)<-"+proj=utm +zone=28 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0"

glmPredObs<-over( pred_glm, obs[,"RIQUEZA"])
glmPredObs<-data.frame(glmPredObs, pred_glm@data)
names(glmPredObs)<-c("Observed", "glm_pred",  "x_km", "y_km")
head(glmPredObs)

glmPredObs75<-over( pred_glm, dati_sampled.spdf[,"ASR_rich"])
glmPredObs75<-data.frame(glmPredObs75, pred_glm@data)
names(glmPredObs75)<-c("Observed", "glm_pred",  "x_km", "y_km")
head(glmPredObs75)

glmPredObs25<-over( pred_glm, test_df.spdf[,"ASR_rich"])
glmPredObs25<-data.frame(glmPredObs25, pred_glm@data)
names(glmPredObs25)<-c("Observed", "glm_pred",  "x_km", "y_km")
head(glmPredObs25)

summary(lm(glmPredObs$Observed~glmPredObs$glm_pred))
summary(lm(glmPredObs75$Observed~glmPredObs$glm_pred))
summary(lm(glmPredObs25$Observed~glmPredObs$glm_pred))

par(mfrow=c(1,3))
plot(glmPredObs$Observed, glmPredObs$glm_pred     , xlab="Observed", ylab="Predicted", main="Observed vs GLM Predicted")
abline(lm(glmPredObs$Observed~glmPredObs$glm_pred), col="red")
abline(0,1, lty=2)
text(5,20, "R2=0.291", col="red")
plot(glmPredObs75$Observed, glmPredObs$glm_pred     , xlab="Observed 75%", ylab="Predicted", main="Observed 75% vs GLM Predicted")
abline(lm(glmPredObs75$Observed~glmPredObs$glm_pred), col="red")
abline(0,1, lty=2)
text(5,20, "R2=0.2867", col="red")
plot(glmPredObs25$Observed, glmPredObs$glm_pred     , xlab="Observed 25%", ylab="Predicted", main="Observed 25% vs GLM Predicted")
abline(lm(glmPredObs25$Observed~glmPredObs$glm_pred), col="red")
abline(0,1, lty=2)
text(5,20, "R2=0.2762", col="red")
par(mfrow=c(1,1))

### RMSE, MAE, RSS
glmPredObs
glmPredObs75
glmPredObs25

glm_rmse<-Rmse(na.omit(glmPredObs$Observed-glmPredObs$glm_pred))
glm_rmse75<-Rmse(na.omit(glmPredObs75$Observed-glmPredObs75$glm_pred))
glm_rmse25<-Rmse(na.omit(glmPredObs25$Observed-glmPredObs25$glm_pred))
round(glm_rmse,2)
round(glm_rmse75,2)
round(glm_rmse25,2)

glm_mae<-MAE(na.omit(glmPredObs$Observed-glmPredObs$glm_pred))
glm_mae75<-MAE(na.omit(glmPredObs75$Observed-glmPredObs75$glm_pred))
glm_mae25<-MAE(na.omit(glmPredObs25$Observed-glmPredObs25$glm_pred))
round(glm_mae,2)
round(glm_mae75,2)
round(glm_mae25,2)

#### BGLSM GOF ####
require(raster)
require(sp)
summary(round(results_strict1[,2:6],2))

pred1<-results_strict1[, c(2, 7:8)]
pred1$X<-pred1$centroids.x_km*1000
pred1$Y<-pred1$centroids.y_km*1000
coordinates(pred1) <- ~ X+Y
crs(pred1)<-"+proj=utm +zone=28 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0"

q2.5<-results_strict1[, c(4, 7:8)]
q2.5$X<-pred1$centroids.x_km*1000
q2.5$Y<-pred1$centroids.y_km*1000
coordinates(q2.5) <- ~ X+Y
crs(q2.5)<-"+proj=utm +zone=28 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0"

q97<-results_strict1[, c(6, 7:8)]
q97$X<-pred1$centroids.x_km*1000
q97$Y<-pred1$centroids.y_km*1000
coordinates(q97) <- ~ X+Y
crs(q97)<-"+proj=utm +zone=28 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0"

unc1<-results_strict1[, c(3, 7:8)]
unc1$X<-unc1$centroids.x_km*1000
unc1$Y<-unc1$centroids.y_km*1000
coordinates(unc1) <- ~ X+Y
crs(unc1)<-"+proj=utm +zone=28 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0"

bglsmPredObs<-over( pred1, obs[,"RIQUEZA"])
bglsmPredObs<-data.frame(bglsmPredObs, pred1@data)
names(bglsmPredObs)<-c("Observed", "bglsm_pred",  "x_km", "y_km")
bglsmPredObs$bglsm_pred<-round(bglsmPredObs$bglsm_pred,0)
head(bglsmPredObs)

bglsmPredObs75<-over( pred1, dati_sampled.spdf[,"ASR_rich"])
bglsmPredObs75<-data.frame(bglsmPredObs75, pred1@data)
names(bglsmPredObs75)<-c("Observed", "bglsm_pred",  "x_km", "y_km")
bglsmPredObs75$bglsm_pred<-round(bglsmPredObs75$bglsm_pred,0)
head(bglsmPredObs75)

bglsmPredObs25<-over( pred1, test_df.spdf[,"ASR_rich"])
bglsmPredObs25<-data.frame(bglsmPredObs25, pred1@data)
names(bglsmPredObs25)<-c("Observed", "bglsm_pred",  "x_km", "y_km")
bglsmPredObs25$bglsm_pred<-round(bglsmPredObs25$bglsm_pred,0)
head(bglsmPredObs25)

summary(lm(bglsmPredObs$Observed~bglsmPredObs$bglsm_pred))
summary(lm(bglsmPredObs75$Observed~bglsmPredObs75$bglsm_pred))
summary(lm(bglsmPredObs25$Observed~bglsmPredObs25$bglsm_pred))

par(mfrow=c(1,2))
plot(bglsmPredObs75$bglsm_pred, bglsmPredObs75$Observed, main= "75%")
abline(lm(bglsmPredObs75$Observed~bglsmPredObs75$bglsm_pred))
plot(bglsmPredObs25$bglsm_pred, bglsmPredObs25$Observed,  main= "25%")
abline(lm(bglsmPredObs25$Observed~bglsmPredObs25$bglsm_pred))
par(mfrow=c(1,1))

bglsm_rmse<-Rmse(na.omit(bglsmPredObs$bglsm_pred-bglsmPredObs$Observed))
bglsm_rmse75<-Rmse(na.omit(bglsmPredObs75$bglsm_pred-bglsmPredObs75$Observed))
bglsm_rmse25<-Rmse(na.omit(bglsmPredObs25$Observed-bglsmPredObs25$bglsm_pred))
round(bglsm_rmse,2)
round(bglsm_rmse75,2)
round(bglsm_rmse25,2)

bglsm_mae<-MAE(na.omit(bglsmPredObs$bglsm_pred-bglsmPredObs$Observed))
bglsm_mae75<-MAE(na.omit(bglsmPredObs75$Observed-bglsmPredObs75$bglsm_pred))
bglsm_mae25<-MAE(na.omit(bglsmPredObs25$Observed-bglsmPredObs25$bglsm_pred))
round(bglsm_mae,2)
round(bglsm_mae75,2)
round(bglsm_mae25,2)

#to raster
#gridify your set of points
gridded(pred1) <- TRUE 
gridded(q2.5) <- TRUE 
gridded(q97) <- TRUE 
gridded(unc1) <- TRUE

#convert to raster
r_pred1 <- raster(pred1)
r_q2.5 <- raster(q2.5)
r_q97 <- raster(q97)
r_unc1 <- raster(unc1)

require(rasterVis)
myBreaks= c(0,3,6,9,12,15,18, 21, 24, 27, 30, 33, 36)
myColorkey <- list(at=myBreaks, ## where the colors change
                   labels=list(labels=myBreaks, ##what to print
                               at=myBreaks))    ##where to print

myBreaks2= c(0,5,10,20,30)
myColorkey2 <- list(at=myBreaks2, ## where the colors change
                    labels=list(labels=myBreaks2, ##what to print
                                at=myBreaks2))    ##where to print
r_pred1_recl<-r_pred1
r_pred1_recl[r_pred1_recl>36]<-36
p1_bglsm<-levelplot(r_pred1_recl, par.settings = YlOrRdTheme, 
                  xlab=NULL, ylab=NULL, scales=list(draw=FALSE),
                 colorkey= myColorkey, at = myBreaks,
                  zscaleLog = NULL, contour = FALSE, margin = F)

p1_bglsm

q2.5_bglsm<-levelplot(r_q2.5, par.settings = YlOrRdTheme, 
                    xlab=NULL, ylab=NULL, scales=list(draw=FALSE),
                    colorkey= myColorkey, at = myBreaks,
                    zscaleLog = NULL, contour = FALSE, margin = F)
q2.5_bglsm

r_q97_recl<-r_q97
r_q97_recl[r_q97_recl>36]<-36
q97_bglsm<-levelplot(r_q97_recl, par.settings = YlOrRdTheme, 
                      xlab=NULL, ylab=NULL, scales=list(draw=FALSE),
                      colorkey= myColorkey, at = myBreaks,
                      zscaleLog = NULL, contour = FALSE, margin = F)
q97_bglsm

r_unc1_recl<-r_unc1
r_unc1_recl[r_unc1_recl>30]<-30
p2<-levelplot(r_unc1_recl, par.settings = YlOrRdTheme, 
              xlab=NULL, ylab=NULL, scales=list(draw=FALSE),
              colorkey= myColorkey2, at = myBreaks2,
              zscaleLog = NULL, contour = FALSE, margin = F)

p2

library(gridExtra)
pp<-grid.arrange(q2.5_bglsm, p1_bglsm, q97_bglsm, p2,  nrow= 2, ncol=2)
pp
tiff("D:/OneDrive - UCL/BGLSM_review/BGLSM_October_2018/imgs_NOV2018/predictions.tiff", height = 6, width = 10, res=300, units="in")
plot(pp )
dev.off()

p.stack<-stack(r_q2.5,r_pred1, r_q97, r_unc1)
bwplot(p.stack, scales=list(limits=c(0,1000)))

####plot predictors raster
require(rgdal)
require(raster)
costa<-shapefile("C:/Users/ddare/Dropbox/DA RE Tenerife/Pennisetum/maxent_tenerife/03_Analisi_TFE/LineaCosta_Tenerife_smooth.shp")
predlist<-list.files("C:/Users/ddare/Dropbox/DA RE Tenerife/centraline/", pattern="*.tif", full.names=TRUE)
pred_stack<-stack(predlist[c(1:4)])
summary(pred_stack)
cellStats(pred_stack, stat='mean', na.rm=TRUE)

pred_stack$roads_10km[is.na(pred_stack$roads_10km)]<-0
pred_stack<-mask(pred_stack, costa)
summary(pred_stack)
plot(pred_stack)

names(pred_stack)
them<-rev(brewer.pal(8,"BrBG"))
mapTheme <- rasterTheme(region=them)

p3<-levelplot(pred_stack[[1]],  scales=list(draw=FALSE), contour = FALSE, margin = FALSE,  par.settings = BuRdTheme)#, main= "Precipitations seasonality")
p4<-levelplot(pred_stack[[2]],  scales=list(draw=FALSE), contour = FALSE, margin = FALSE,  par.settings = mapTheme)#, main= "Elevation")
p5<-levelplot(pred_stack[[4]],  scales=list(draw=FALSE), contour = FALSE, margin = FALSE,  par.settings = YlOrRdTheme)#, main= "Road Kernel Density")
p6<-levelplot(pred_stack[[3]],  scales=list(draw=FALSE), contour = FALSE, margin = FALSE,  par.settings = RdBuTheme)#, main= "Spring Precipitations")
# p7<-levelplot(pred_stack[[5]],  scales=list(draw=FALSE), contour = FALSE, margin = FALSE,  par.settings = BuRdTheme, main= "Minimum temperatures \n seasonality")

library(gridExtra)
pp1<-grid.arrange(  p3,p6, p4, p5, ncol=2)
tiff("D:/OneDrive - UCL/BGLSM_review/BGLSM_October_2018/imgs_NOV2018/Predictors.tiff", height = 8, width = 10, res=300, units="in", bg = "white")
plot(pp1) 
dev.off()